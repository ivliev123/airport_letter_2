import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import pickle

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D

from collections import defaultdict
from io import StringIO
# from matplotlib import pyplot as plt
from PIL import Image

# import load_class
import cv2


# import cv2
import io
# import socket
import struct
import time
# import pickle
import zlib

# client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# client_socket.connect((socket.gethostname(), 8485))
# connection = client_socket.makefile('wb')


img_counter = 0
encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 50]



cap = cv2.VideoCapture(1)

# This is needed since the notebook is stored in the object_detection folder.
# sys.path.append("..")
sys.path.append("/home/ivliev/models/research/object_detection/")

from utils import label_map_util
from utils import visualization_utils as vis_util

sys.path.append("..")



# What model to download.
MODEL_NAME = '/media/ivliev/9016-4EF8/airport_letter_2/workspace/training_demo/trained-inference-graphs/output_inference_graph_v8.pb'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'
# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('annotations', 'label_map.pbtxt')
NUM_CLASSES = 37


# ## Load a (frozen) Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')


# ## Loading label map
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


# ## Helper code
def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)





# In[10]:

with detection_graph.as_default():
  with tf.Session(graph=detection_graph) as sess:
    while True:
      ret, image_np = cap.read()
      # for i in range(300):
      # image_np = cv2.imread('/media/ivliev/F045-E632/signs_photo/'+str(i)+'.jpg')
      # image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
      image_np_expanded = np.expand_dims(image_np, axis=0)
      image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
      # Each box represents a part of the image where a particular object was detected.
      boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
      # Each score represent how level of confidence for each of the objects.
      # Score is shown on the result image, together with the class label.
      scores = detection_graph.get_tensor_by_name('detection_scores:0')
      classes = detection_graph.get_tensor_by_name('detection_classes:0')
      num_detections = detection_graph.get_tensor_by_name('num_detections:0')
      # Actual detection.
      (boxes, scores, classes, num_detections) = sess.run(
          [boxes, scores, classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})

      # Visualization of the results of a detection.
      # cut_sign(image_np, boxes, scores)

      frame = image_np
      result, frame = cv2.imencode('.jpg', frame, encode_param)
      #    data = zlib.compress(pickle.dumps(frame, 0))
      frame_s_array = [frame, boxes, scores]
      data = pickle.dumps(frame_s_array, 0)
      size = len(data)
      # print("{}: {}".format(img_counter, size))
      # client_socket.sendall(struct.pack(">L", size) + data)

      vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=3)

          # cv2.imwrite('/media/ivliev/F045-E632/image_np'+str(i)+'.jpg',image_np)

      cv2.imshow('object detection', cv2.resize(image_np, (800,600)))
      if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
